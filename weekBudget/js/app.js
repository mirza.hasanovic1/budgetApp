// Variables
const expenseForm = document.getElementById("add-expense"),
      budgetTotal = document.querySelector("span#total"),
      budgetLeft = document.querySelector("span#left");
let userBudget,budgetInput;
// Classes
class Budget {
  
  constructor(amount) {
    this.budget = Number(amount);
    this.budgetLeft = this.budget;
  }

  subtractFromBudget(amount) {
    return this.budgetLeft -= amount;
  }
}
class HTML {
  
  insertBudget(amount) {
    budgetTotal.innerHTML = `${amount}`;
    budgetLeft.textContent = amount;
  }

  printMessage(message, className) {
    const messageDiv = document.createElement("div");
    messageDiv.classList.add("text-center", "alert", className);
    messageDiv.appendChild(document.createTextNode(message));
    document.querySelector(".primary").insertBefore(messageDiv, expenseForm);
    setTimeout(function() {
      messageDiv.remove();
      expenseForm.reset();
    }, 3000);
  }

  addExpenseToList(name, amount) {
    const list = document.querySelector("ul");
    const li = document.createElement("li");
    li.className = "list-group-item d-flex justify-content-between align-items-center";
    li.innerHTML = `
      ${name}
      <span class="badge badge-primary badge-pill">$${amount}</span>
    `;
    list.appendChild(li);
  }

  trackBudget(amount) {
    const budgetLeftDollars = budgetInput.subtractFromBudget(amount);
    budgetLeft.innerHTML = `${budgetLeftDollars}`;
    if(budgetLeftDollars <= budgetInput.budget * 0.5) {
      budgetLeft.parentElement.parentElement.classList.remove("alert-success", "alert-warning");
      budgetLeft.parentElement.parentElement.classList.add("alert-danger");
    }
    if(budgetLeftDollars <= budgetInput.budget * 0.25) {
      budgetLeft.parentElement.parentElement.classList.remove("alert-success");
      budgetLeft.parentElement.parentElement.classList.add("alert-warning");
    }
  }
}
const html = new HTML();
// Event Listeners
enventListeners();

function enventListeners() {
  
  document.addEventListener("DOMContentLoaded", function() {
    userBudget = prompt("What is your budget for this week?");
    if(userBudget == null || userBudget == '' || userBudget == '0')
      window.location.reload();
    else{
      budgetInput = new Budget(userBudget);
      html.insertBudget(budgetInput.budget);
    }
  })
  
  expenseForm.addEventListener("submit", function(e) {
    e.preventDefault();
    const expenseName = document.querySelector("input#expense").value;
    const expenseAmount = document.querySelector("input#amount").value;
    if(expenseName === '' || expenseAmount === '') {
      html.printMessage("All fields are mandatory, try again!", "alert-danger");
    }
    else {
      html.addExpenseToList(expenseName, expenseAmount);
      html.trackBudget(expenseAmount);
      html.printMessage("Added...", "alert-success");
    }
  })
}